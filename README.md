# Kolmeo Slides

A quick and easy Markdown-to-Slides deck with a Kolmeo theme.

## Prerequisites

- NodeJS

## Setup

- Clone this repo with one of:
  - `degit bitbucket:dan-simpson/kolmeo-slides <MY-SLIDE-DECK-NAME>`, or
  - `git clone --depth 1 git@bitbucket.org:dan-simpson/kolmeo-slides.git <MY-SLIDE-DECK-NAME>`
- Remove the .git subfolder
- Inside your new directory:
  - `npm start` / `npm run develop` => development flow with hot reloading. Double click on the emitted `src/*.html` to open in browser
  - `npm run build` => build and archive your presentation

## Gotchas

- Make sure you download any and all media you need to use into the `media` folder.  If you don't and you're trying to present offline, things will break.