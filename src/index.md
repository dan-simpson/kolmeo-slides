---
marp: true
theme: kolmeo
---

# Hello!

Welcome to the example slide

Preview me in visual studio code using `Ctrl+Shift+V`

---

<!-- _class: lead -->

## Content centered vertically?

- Just add a little `<!-- _class: lead -->`

---

![bg right:33% grayscale](media/background.jpg)

## Want a grayscale background on one side only?

And fall over to the default one on the other side, obvs.

You could have set the whole background using just
`![bg](your image path)`

if thats you're thing too

---

## Bullets

- you can add
- as many 
- bullet points
  - at any hierarchy
- as you like

---

## Comments == Presenter notes

Adding something like

```
<!-- This is my note -->
```

will show up in presenter mode only

<!-- This is my note -->

---

<!-- _class: columns -->
## Column layout

You can add columns in a slightly hacky way using `<!-- _class: columns -->`

- First level becomes a  title
  - then second level
  - are your
  - dot points
- 
  - As you can see here, you can skip the top level for a heading-less column
-
  - Though it works better if there is more than one

---

## Build it

- By running `npm run marp:start`, you can build your resulting HTML file and watch for local changes
- By running `npm run marp:build`, it will build without the watch.

Run the resulting HTML file in a browser, and see what happens!

> ### You have been warned:
>
> Note! You should use local assets where possible (in the local media directory).  If you build a presentation that references external images, these won't magically show up if you have no internet!

---

## Thanks for coming to my TED talk
